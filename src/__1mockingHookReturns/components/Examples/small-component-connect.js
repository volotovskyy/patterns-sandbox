import React, { useEffect } from 'react';
import cancan from 'cancan';
import { compose } from 'redux';
import { connect } from 'react-redux';
import LayerAPIWrapper from 'tzcommons/components/Layers/LayerAPIWrapper';
import { getSetting } from 'tzredux/lib/settings.reducer';

function UpdatePasswordToggle({
    passwordSnooze,
    mountLayer,
    unmountLayer,
}) {
    useEffect(() => {
        const user = cancan.getUser();
        const showModal = (
            user &&
            user['password-meta'] &&
            user['password-meta'].score < 3 && (
                !passwordSnooze ||
                Date.now() > passwordSnooze
            )
        );
        if (showModal) {
            // Load modal async to reduce size of initial chunk
            import(/* webpackChunkName: "PasswordUpdateModal" */ './Modal').then((modalModule) => {
                const PasswordUpdateModal = modalModule.default;
                mountLayer(
                    <PasswordUpdateModal closeModal={() => unmountLayer('modals')} />,
                    'modals',
                    'update_password_modal',
                );
            });
        }
    }, [passwordSnooze, mountLayer, unmountLayer]);

    return false;
}

const mapState = (state) => ({
    passwordSnooze: getSetting(state, 'password-snooze'),
});

export default compose(
    connect(mapState),
    LayerAPIWrapper,
)(UpdatePasswordToggle);