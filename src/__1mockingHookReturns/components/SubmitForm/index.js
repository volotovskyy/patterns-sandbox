import React from "react";
import { useSubmitAction } from "./resources/useSubmitAction";

export const SubmitForm = () => {
  const [response, performSubmit] = useSubmitAction();
  console.log("→ response", response);
  return (
    <section>
      <button disabled={response.type === "Pending"} onClick={performSubmit}>
        Submit
      </button>
      {response.type === "Success" ? (
        <div>
          <p>Operation was successful. Response:</p>
          <br />
          <pre>{JSON.stringify(response.result, null, 2)}</pre>
        </div>
      ) : null}
      {response.type === "Failure" ? (
        <p>An error occured: {response.error.message}.</p>
      ) : null}
    </section>
  );
};
