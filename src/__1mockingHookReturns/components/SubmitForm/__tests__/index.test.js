import React from "react";
import ShallowRenderer from "react-test-renderer/shallow";

import { SubmitForm } from "../index.js";
import { useSubmitAction } from "../resources/useSubmitAction";
jest.mock("../resources/useSubmitAction.js");

test("form rendering", () => {
  const renderer = new ShallowRenderer();
  const cb = jest.fn();
  const error = { message: "Mocked error" };

  useSubmitAction.mockReturnValue([{ type: "Failure", error }, cb]);
  renderer.render(<SubmitForm />);

  const result = renderer.getRenderOutput();
  expect(result).toMatchInlineSnapshot(`
    <section>
      <button
        disabled={false}
        onClick={[MockFunction]}
      >
        Submit
      </button>
      <p>
        An error occured: 
        Mocked error
        .
      </p>
    </section>
  `);
});
