import { useAction } from "../../../hooks/useAction";

async function submitForm() {
  return fetch("https://www.reddit.com/r/reactjs.json?limit=10").then(
    response => response.json()
  );
  // fetch('https://www.reddit.com/r/reactjs.json?limit=10')
  //     .then(response => {
  //       if (!response.ok) {
  //         throw new Error("Failed to fetch.");
  //       }
  //       return response.json();
  //     })
}

export const useSubmitAction = () => {
  return useAction(submitForm);
};
