import React from "react";
import { useUserProfile } from "./resources/useUserProfile";

export const UserProfile = () => {
  const profile = useUserProfile();

  return <p>{profile.name}</p>;
};
