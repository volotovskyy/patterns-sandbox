import React from "react";
import ShallowRenderer from "react-test-renderer/shallow";

import { UserProfile } from "../index.js";
import { useUserProfile } from "../resources/useUserProfile";
jest.mock("../resources/useUserProfile.js");

test("profile rendering", () => {
  const renderer = new ShallowRenderer();

  useUserProfile.mockReturnValue({ name: "Liza", age: 28 });
  renderer.render(<UserProfile />);

  const result = renderer.getRenderOutput();
  expect(result).toEqual(<p>Liza</p>);
});
