import { useState, useCallback } from "react";

const idle = Object.freeze({ type: "Idle" });
const pending = Object.freeze({ type: "Pending" });

export const useAction = fn => {
  const [response, setResponse] = useState(idle);

  const action = useCallback(
    (...args) => {
      setResponse(pending);
      Promise.resolve(fn(...args))
        .then(result => setResponse({ type: "Success", result }))
        .catch(error => setResponse({ type: "Failure", error }));
    },
    [fn]
  );

  return [response, action];
};
