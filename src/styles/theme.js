export const colors = {
  red: "tomato",
  blue: "lightblue",
  green: "green"
};

export const space = [
  // margin and padding
  0,
  4,
  8,
  16,
  32,
  64,
  128,
  256
];

export const fontSize = [12, 14, 16, 24, 32, 48, 64, 96, 128];

export const theme = {
  colors,
  space,
  fontSize
};
