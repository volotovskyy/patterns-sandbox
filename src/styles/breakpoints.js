export const BREAKPOINTS = Object.freeze({
  xs: {
    min: null,
    max: 767
  },
  sm: {
    min: 768,
    max: 991
  },
  md: {
    min: 992,
    max: 1199
  },
  lg: {
    min: 1200,
    max: 1499
  },
  xl: {
    min: 1500,
    max: 1919
  },
  xxl: {
    min: 1920,
    max: null
  }
});
