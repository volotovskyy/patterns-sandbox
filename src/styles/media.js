// vendor
import { css } from "styled-components";

// own
import { BREAKPOINTS } from "./breakpoints";

// iterate through the sizes and create a media template
export const media = Object.keys(BREAKPOINTS).reduce((acc, label) => {
  const minSize = BREAKPOINTS[label].min;
  const maxSize = BREAKPOINTS[label].max;
  acc[label] = (...args) => css`
        @media ${minSize && `(min-width: ${minSize}px)`} ${minSize &&
    maxSize &&
    "and"} ${maxSize && `(max-width: ${maxSize}px)`} {${css(...args)};
        }
    `;

  return acc;
}, {});

//
// media util usage for one breakpoint
//
// ${media.xl`width: 50%`};

// ${media.lg`
//    width: 25%;
//    padding: 24px;
// `};

// we can't go with something like ${media.md - media.xxl`your css`}
// so here is default media usage for multiple breakpoints cases
//
// @media (min-width: ${minSize}px) and (max-width: ${maxSize}px) {
