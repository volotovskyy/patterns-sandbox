// useless
export const requestWithRedux = async (options, callback) => {
  const {
    parent,
    method: sendRequest,
    dispatch,
    action,
    payload: clientPayload,
    errorAction
  } = options;

  dispatch({
    type: parent
  });

  const response = await sendRequest(clientPayload);
  const serverPayload = await response.json();

  try {
    if (response.status !== 200) {
      throw new Error(serverPayload.message);
    }
  } catch (error) {
    dispatch(errorAction(error));

    return false;
  }

  if (typeof callback === "function") {
    const data = callback(serverPayload);

    dispatch(action(data));
  } else {
    dispatch(action(serverPayload));
  }
};


// // Types
// import { types } from './types';
//
// // Instruments
// import { api } from '../../REST';
// import { requestWithRedux as request } from '../../utils/requestWithRedux';
// import { groupRequestWithRedux } from "../../utils/groupRequestWithRedux";
//
// export const postsActions = {
//     // Sync
//     fillPosts: (posts) => {
//         return {
//             type:    types.FILL_POSTS,
//             payload: posts,
//         };
//     },
//     createPost: (post) => {
//         return {
//             type:    types.CREATE_POST,
//             payload: post,
//         };
//     },
//     clearPosts: () => {
//         return {
//             type: types.CLEAR_POSTS,
//         };
//     },
//
//     // Async
//     fetchPostsAsync: () => async (dispatch) => {
//         // dispatch({
//         //     type: types.FETCH_POSTS_ASYNC,
//         // });
//
//         // const response = await api.posts.fetch();
//         // const result = await response.json();
//
//         // dispatch(postsActions.fillPosts(result.data));
//         const options = {
//             parent: types.FETCH_POSTS_ASYNC,
//             method: api.posts.fetch,
//             dispatch,
//             action: postsActions.fillPosts,
//         };
//
//         await request(options, (payload) => payload.data);
//     },
//     printError: (message) => {
//         console.log(`Error: ${message}`);
//     },
//     createPostAsync: (comment) => async (dispatch) => {
//         const options = {
//             parent:      types.CREATE_POST_ASYNC,
//             method:      api.posts.create,
//             dispatch,
//             action:      postsActions.createPost,
//             errorAction: postsActions.printError,
//             payload:     {
//                 comment,
//             },
//         };
//
//         try {
//             await request(options, (payload) => payload.data);
//         } catch (e) {
//             // Dispatch another action;
//         }
//         // return {
//         //     type:    types.CREATE_POST_ASYNC,
//         //     payload: comment,
//         // };
//     },
//     groupPostsAsync: () => async (dispatch) => {
//         const options = {
//             parent:  types.MAKE_GROUP_POSTS_ASYNC,
//             methods: [
//                 {
//                     api:    api.posts.create,
//                     action: postsActions.createPostAsync,
//                 },
//                 {
//                     api:    api.posts.fetch,
//                     action: postsActions.fetchPostsAsync,
//                 }
//             ],
//             dispatch,
//             action: postsActions.fillPosts,
//         };
//
//         await groupRequestWithRedux(options);
//     },
// };
