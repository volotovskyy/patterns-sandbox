/*
    Main reducer & store.

    STRUCTURE
    The flux structure should follow the pattern proposed in "Ducks: Redux Reducer Bundles":
    https://github.com/erikras/ducks-modular-redux


    ACTION CREATORS
    Actions & action creators should follow the pattern proposed by "Flux Standard Action":
    https://github.com/acdlite/flux-standard-action

*/

import app from "tzredux/lib/global.reducer";
import settings from "tzredux/lib/settings.reducer";
import asyncReducer, {
  setListener as setAsyncListener
} from "tzredux/lib/async.reducer";
import forurl from "tzredux/lib/forurl.reducer";
// import { reducerWrapper } from "tzredux/lib/activate-reducer";

// Utils
// import showMessage from 'utils/messages';

// View Reducers
// import agreementsImport from 'pages/Agreements/utils/agreements-import.reducer';

const legacyReducers = {
  app,
  async: asyncReducer,
  forurl,
  settings

  // Views
  // agreementsImport: reducerWrapper(agreementsImport, 'agreementsImport'),
};

export function initialize() {
  function asyncListener(err, result) {
    if (err) {
      console.log("ERROR asyncListener:", err);
    } else {
      console.log("ERROR asyncListener:", result);
    }
    // if (err) {
    //     showMessage({
    //         type: 'danger',
    //         msg: err,
    //     });
    // } else {
    //     showMessage({
    //         type: 'success',
    //         msg: result,
    //     });
    // }
  }
  setAsyncListener(asyncListener);
}

export default legacyReducers;
