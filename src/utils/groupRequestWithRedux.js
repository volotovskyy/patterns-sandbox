export const groupRequestWithRedux = async (options, callback) => {
    const {
        parent: parentAction,
        methods,
        dispatch,
        payload: clientPayload,
        errorAction,
    } = options;

    dispatch(parentAction());

    // const sendRequest = methods[0][0];
    // const response = await sendRequest(clientPayload);
    // const serverPayload = await response.json();
    // dispatch(methods[0][1](serverPayload));
    const promises = methods.map((method) => {
        return method[0]().then(async (data) => {
            const serverPayload = await data.json();
            console.log(serverPayload);
            dispatch(method[1](serverPayload));
        });
    });

    // console.log(promises);

    // Promise.all([methods[0][0]()]).then(async (data) => {
    //     for (const item of data) {
    //         const serverPayload1 = await item.json();
    //         dispatch(methods[0][1](serverPayload1));
    //     }
    // });

    //
    // try {
    //     if (response.status !== 200) {
    //         throw new Error(serverPayload.message);
    //     }
    // } catch (error) {
    //     dispatch(errorAction(error));
    //
    //     return false;
    // }
    //
    // if (typeof callback === 'function') {
    //     const data = callback(serverPayload);
    //
    //     dispatch(action(data));
    // } else {
    //     dispatch(action(serverPayload));
    // }
};
