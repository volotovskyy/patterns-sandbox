export const request = async (sendRequest, clientPayload) => {
  const response = await sendRequest(clientPayload)();

  const serverPayload = await response.json();
  if (response.status !== 200) {
    throw new Error(serverPayload.message);
  }

  return serverPayload;
};
