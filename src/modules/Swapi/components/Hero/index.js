import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { groupRequestWithRedux } from '../../../../utils';
import styled from 'styled-components';

import { actions } from "modules/Swapi/state/actions";
import { api } from "modules/Swapi/state/api";
import { request } from "utils";

export const Hero = props => {
  const dispatch = useDispatch();

  // const fetchHero = () => {
  //   console.log("→ fetch!!");
  //   dispatch(actions.someThunkAction());
  // };
  // useEffect(() => {
  //   dispatch(actions.someThunkAction);
  // }, [dispatch]);

  const sendGroupRequest = () => {
    const options = {
      parent: actions.someGroupAction,
      dispatch,
      methods: [
        [api.getData, actions.printHeroAction],
        [api.getData, actions.printHeroAction]
      ]
    };

    groupRequestWithRedux(options);
  };

  return <div onClick={ sendGroupRequest }>Hero</div>;
};

const HeroCard = styled.div`
  width: 150px;
  height: 200px;
  background: snow;
  display: flex;
  align-items: center;
  justify-content: center;
`;
