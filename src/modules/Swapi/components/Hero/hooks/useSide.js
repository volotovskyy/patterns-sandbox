import { useEffect, useReducer } from "react";

export const useSide = () => {
  const [state, setState] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    {
      loading: true,
      isDarkSide: false
    }
  );

  useEffect(() => {
    const apiCall = async () => {};
    const response = apiCall();
    setState({ loading: false, isDarkSide: response });
  }, [state.isDarkSide, state.loading]);
};

// usage:
//
// const [isActive, isLoading] = useSide(side)
//
// if (isLoading) return <Loader /> || null
//
// return ()
