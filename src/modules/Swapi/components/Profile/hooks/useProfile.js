import { useState, useEffect } from 'react';
import { request } from '../../../../../utils';

export const useProfile = (gender, api) => {
    const [name, setName] = useState({ title: "", first: "", last: "" });

    useEffect(
        () => {
            (async () => {
                const profile = await request(api, 'male');
                setName(profile.results[0].name);
            })();
        },
        [gender]
    );

    return name;
};
