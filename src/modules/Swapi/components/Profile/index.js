import React from 'react';

import { useProfile } from './hooks/useProfile';
import { api } from '../../api';

export const Profile = ({ gender }) => {
    const { first, last } = useProfile(gender, api.getProfile);

    return (
        <>
            <h1>Profile</h1>
            <p>{ first }</p>
            <p>{ last }</p>
        </>
    );
};
