import { createSelector } from "reselect";

export const getHero = state => state.swapi.hero;
