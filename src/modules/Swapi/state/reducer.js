import { types } from "./types";

const ReducerState = {
  hero: {}
};

export const swapi = (state = ReducerState, action) => {
  const { type, payload } = action;

  switch (type) {
    case types.FETCH_HERO:
      return {
        ...state,
        hero: { ...payload }
      };

    default:
      return state;
  }
};
