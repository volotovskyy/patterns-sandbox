// tz
import { makeFetch } from "state/store/helpers/actionCreatorFactories";

// own
import { types } from "./types";
import { api } from "./api";

export const actions = Object.freeze({
  someAction: () => ({
    type: types.SIMPLE_ACTION
  }),
  // inline
  someRequestAction: data => (dispatch, getState) => {
    // makeRequest(api.specialCall)(dispatch, getState);
  },
  // redux
  someThunkAction: () => (dispatch, getState) => {
    // makeFetch = (collection, uri, options = {}, clientId) => (dispatch, getState) =>
    // makeFetch("swapi", "/people/1")(dispatch, getState);
    makeFetch("pokemon", "/pokemon/ditto/")(dispatch, getState);
  },
  // group
  someGroupAction: () => ({
    type: types.SOME_GROUP_ACTION
  }),
  printHeroAction: (payload) => ({
    type: types.FETCH_HERO,
    payload
  })
});
