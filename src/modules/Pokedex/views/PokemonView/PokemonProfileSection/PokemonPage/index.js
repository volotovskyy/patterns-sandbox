import React, { Suspense } from "react";
import { useParams } from 'react-router-dom';
// import { useSuspense } from "hooks";

// import { fetchPokemon } from '../state/actions'

// import { PokeList } from "./components/PokeList";
import { PokeProfile } from "../components/PokeProfile";


export const PokemonPage = () => {
    const { pokemonName } = useParams();
    return (
      <Suspense fallback={<p>Loading {pokemonName} profile…</p>}>
        <PokeProfile name={pokemonName} />
      </Suspense>
    );
  }