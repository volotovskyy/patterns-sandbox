import React, { Fragment } from 'react';

export const PokemonInfo = ({ name }) => {
    const pokemon = useQuery(Pokemon, name);
    return (
      <Fragment>
        {/* <Img src={pokemon.sprites.front_default} /> */}
        <p>
          <strong>{pokemon.name}</strong>
        </p>
        <dl>
          {pokemon.stats.map(datum => (
            <Fragment key={datum.stat.name}>
              <dt>{datum.stat.name}</dt>
              <dd>{datum.base_stat}</dd>
            </Fragment>
          ))}
        </dl>
      </Fragment>
    );
  }