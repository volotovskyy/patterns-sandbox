import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { PokemonProfilePage } from './PokemonPage'

export const PokemonProfileSection = () => {
    return (
        <section className="pokemon-profile">
        <Switch>
            <Route exact path="/:pokemonName">
            <PokemonProfilePage />
            </Route>
            <p>Please select a pokemon from the list.</p>
        </Switch>
        </section>
    );
}