import React from 'react';

export const PokemonListSkeleton = ({ limit }) => {
    const list = Array.from(Array(limit), (_, i) => i + 1);
    
    return (
        <ul>
        {list.map(index => (
            <li key={index}>
            <a href="#" className="skeleton">
                {'x'.repeat(Math.random() * 10 + 5)}
            </a>
            </li>
        ))}
        </ul>
    );
}