import React from 'react';
import { NavLink } from 'react-router-dom';

export const PokemonList = ({ offset, limit }) => {
    const pokemons = useQuery(Pokemons, { offset, limit });

    return (
        <ul>
        {pokemons.map(pokemon => (
            <li key={pokemon.name}>
            <NavLink to={`/${pokemon.name}`}>{pokemon.name}</NavLink>
            </li>
        ))}
        </ul>
    );
}