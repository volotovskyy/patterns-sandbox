import React, { Suspense, useState } from 'react';

import { PokemonList } from './components/PokemonList';
import { PokemonListSkeleton } from './components/PokemonListSkeleton';

export const PokemonListSection = () => {
    const [{ offset, limit }, setParams] = useState({ offset: 0, limit: 10 });

    return (
        <section className="pokemon-list">
        <Suspense fallback={<PokemonListSkeleton limit={limit} />}>
            <PokemonList offset={offset} limit={limit} />
        </Suspense>
        <div className="pokemon-pagination">
            <button
            onClick={() => setParams({ offset: offset - limit, limit })}
            disabled={offset === 0}
            >
            &larr;
            </button>
            <button onClick={() => setParams({ offset: offset + limit, limit })}>
            &rarr;
            </button>
        </div>
        </section>
    );
}

