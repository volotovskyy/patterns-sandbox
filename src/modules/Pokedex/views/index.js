import React from 'react';
import { BrowserRouter } from 'react-router';

import { PokemonListSection } from './PokemonListSection'
import { PokemonProfileSection } from './PokemonProfileSection'


export const PokemonView = (props) => {
    
    return (
        <BrowserRouter>
        <article className="pokedex-grid">
          <PokemonListSection />
          <PokemonProfileSection />
        </article>
      </BrowserRouter>
    )
}