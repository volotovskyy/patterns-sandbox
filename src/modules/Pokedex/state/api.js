const TIMEZYNK_REST = "https://pokeapi.co/api/v2";

export const API = {
  fetchPokemon: pokemon => () => {
    return fetch(`${TIMEZYNK_REST}/pokemon/${pokemon}`);
  }
};
