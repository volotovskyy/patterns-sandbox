// tz
import { makeFetch } from "state/store/helpers/actionCreatorFactories";

// own
import { types } from "./types";
import { api } from "./api";

export const actions = Object.freeze({
  // redux
  fetchPokemon: () => (dispatch, getState) => {
    // makeFetch = (collection, uri, options = {}, clientId) => (dispatch, getState) =>
    // makeFetch("pokemons", "/pokemon/1")(dispatch, getState);
    makeFetch("pokemons", "/pokemon/1/")(dispatch, getState);
  },
  fetchPokemonsList: () => (dispatch, getState) => {
    // makeFetch = (collection, uri, options = {}, clientId) => (dispatch, getState) =>
    // makeFetch("pokemons", "/pokemon/1")(dispatch, getState);
    makeFetch("pokemons", "type/3/")(dispatch, getState);
  }
});
