import React, { Suspense } from 'react'
import { LazySuspended } from './components/LazySuspended'
const OtherComponent = React.lazy(() => import('./OtherComponent'));

export function MyComponent() {
  return (
    <div>
      <Suspense fallback={<div>Loading...</div>}>
        <OtherComponent />
      </Suspense>
    </div>
  );
}