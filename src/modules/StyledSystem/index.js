import React from "react";
import styled from "styled-components";

import {
  space,
  color,
  fontSize,
  width,
  fontWeight,
  lineHeight
} from "styled-system";

const Box = styled.div`
  ${space}
  ${width}
  ${fontSize}
  ${color}
`;
Box.propTypes = {
  ...space.propTypes,
  ...width.propTypes,
  ...fontSize.propTypes,
  ...color.propTypes
};

const Text = styled.div`
  ${space}
  ${fontSize}
  ${fontWeight}
  ${lineHeight}
  ${color}
`;
Text.propTypes = {
  ...space.propTypes,
  ...fontSize.propTypes,
  ...fontWeight.propTypes,
  ...lineHeight.propTypes,
  ...color.propTypes
};

const Heading = Text.withComponent("h1");

Heading.defaultProps = {
  fontSize: 5,
  lineHeight: 1.5,
  m: 0
};

export const StyledSystem = props => {
  return (
    <Box px={[3, 4]} py={[5, 6]} color="white" bg={["blue", "red"]}>
      <Heading fontSize={[4, 5, 6]}>styled-system</Heading>
      <Text fontWeight="bold">Basic demo</Text>
    </Box>
  );
};
