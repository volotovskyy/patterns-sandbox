/*
 * This middleware is a thunk, passing dispatch and getState to actions that are
 * functions. It also handles API requests, identified by the existence of a
 * promise in the action object. It must also have a types array with request,
 * success and fail action types. The middleware dispatches the request action
 * and then attempts to resolve the promise. Depending on the API response/error,
 * the success or fail action is dispatched.
 */
const getId = (function idGetter() {
  let id = 0;
  return () => `req:${++id}`;
})();

function processResult(action) {
  return action;
}

function _apiClientMiddleware({ dispatch, getState }) {
  return next => action => {
    if (typeof action === "function") {
      return action(dispatch, getState);
    }

    if (!action.promise) {
      return next(action);
    }

    const { promise, types, meta } = action;
    const _meta = { ...meta, requestId: getId() };
    const [REQUEST, SUCCESS, FAILURE] = types;
    next({ type: REQUEST, meta: _meta || undefined });

    const value = typeof promise === "function" ? promise() : promise;
    const resolver = Promise.resolve(value);
    const onSuccess = meta.success || processResult;
    const onFail = meta.fail || processResult;

    return resolver.then(
      result => {
        const successAction = onSuccess({
          type: SUCCESS,
          payload: result,
          meta: _meta || undefined
        });
        next(successAction);
        return result;
      },
      error => {
        const failAction = onFail({
          type: FAILURE,
          payload: error,
          error: true,
          meta: _meta || undefined
        });
        next(failAction);
        return resolver;
      }
    );
  };
}

export default function apiClientMiddleware() {
  return _apiClientMiddleware;
}
