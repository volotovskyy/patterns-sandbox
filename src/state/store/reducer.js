/*
 * Here we combine the module reducers into one reducer
 */
// import Immutable from "immutable";
import { combineReducers } from "redux";
// import grouping, { parseData as deserializeGrouping } from "tzgrouping";
// import legacyReducers, { initialize } from "utils/redux/redux.reducers";

// import data from "tzredux/lib/data";
// import selected from "tzredux/lib/selected";
// import progress from "tzredux/lib/progress";
// import settings from "tzredux/lib/view-settings";

import { swapi } from "modules/Swapi/state/reducer";

export default combineReducers({
  // ...legacyReducers,
  swapi
});

// initialize();
