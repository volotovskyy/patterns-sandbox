/*
    Batch actions. Inspired by https://github.com/reactjs/redux/issues/911#issuecomment-149192251

    Dispatch multiple actions but only triggering one state change.
*/

const BATCH_ACTIONS = "tz@BATCH_ACTIONS";

/*
    Reducer wrapper
    // usage
    let store = createStore(enableBatching(reducer));
*/

export default function enableBatching(reducer) {
  return function batchingReducer(state, action) {
    if (typeof action === "function") {
      throw "Cannot run thunk inside batch action!";
    } else if (Array.isArray(action)) {
      throw "Illegal batch syntax";
    } else if (action && action.type === BATCH_ACTIONS) {
      return action.actions.reduce(batchingReducer, state);
    }
    return reducer(state, action);
  };
}

function isThunk(action) {
  return typeof action === "function";
}

/*
    Action batcher.

    // usage
    store.dispatch(
        batchActions(
            doSomething(),
            doSomethingElse()
        )
    );
*/
export function batchActions(...actions) {
  if (!actions) {
    return false;
  }
  if (actions.length === 1 && Array.isArray(actions[0])) {
    actions = actions[0];
  }
  const hasThunk = actions.some(isThunk);
  if (hasThunk) {
    console.log(
      "WARNING: Thunk inside batch action, reverting to serial execution"
    );
    return function(dispatch) {
      actions.forEach(dispatch);
    };
  }

  // No thunks, proceed with normal batching
  return {
    type: BATCH_ACTIONS,
    actions: actions
  };
}
