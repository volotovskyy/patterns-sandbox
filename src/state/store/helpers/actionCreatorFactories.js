/*
 * The action creator factories wrap the methods in async.js and adapt them to
 * work with the apiClientMiddleware.
 *
 */

import * as actions from "./actionTypes";
import { fetch, create, update, destroy, archive } from "./async";
import createUrl from "./createUrl";

// Base

function get({ url, query, types, meta }) {
  return {
    types,
    meta,
    promise() {
      return fetch(url + (query || ""), meta);
    }
  };
}

function post({ url, types, meta, bodyInput }) {
  return {
    types,
    meta,
    promise() {
      return create(url, bodyInput, meta);
    }
  };
}

function put({ url, types, meta, bodyInput }) {
  return {
    types,
    meta,
    promise() {
      return update(url, bodyInput, meta);
    }
  };
}

function putArchive({ url, types, meta, bodyInput, restore }) {
  return {
    types,
    meta,
    promise() {
      return archive(url, bodyInput, meta, restore);
    }
  };
}

function del({ url, types, meta, id }) {
  return {
    types,
    meta,
    promise() {
      return destroy(url, id, meta.data, meta.vid);
    }
  };
}

// Get any dynamic filter values.
function prepFilters(state, filters) {
  let prepped = filters;
  if (filters && typeof filters === "object" && !filters.__raw_query) {
    for (const f in filters) {
      const fn = filters[f];
      if (filters.hasOwnProperty(f) && typeof fn === "function") {
        prepped = {
          ...prepped,
          [f]: fn(state)
        };
      }
    }
  }

  return prepped;
}

/*
    Action creator factories. Takes 3 arguments; collection, url, and filters (optional).
    Filter values can be a function that recieves the current state as argument
    and should return the filter value.

    Use:

    const fetchShiftsByInterval = makeFetch('shifts', '/shifts', {
        from : (state) => getFrom(state),
        to : (state) => getTo(state)
    });

*/

export const makeFetch = (collection, uri, options = {}, clientId) => (
  dispatch,
  getState
) => {
  const { filter = {}, ...other } = options;
  const url = createUrl(uri, prepFilters(getState(), filter));
  const types = [
    actions.CRUD_FETCH,
    actions.CRUD_FETCH_SUCCESS,
    actions.CRUD_FETCH_FAIL
  ];
  const meta = {
    verb: "get",
    collection,
    clientId,
    url,
    filter,
    ...other
  };

  return dispatch(get({ url, types, meta, filter }));
};

export const makeCreate = (
  collection,
  uri,
  options = {},
  bodyInput,
  clientId
) => dispatch => {
  const url = createUrl(uri);
  const types = [
    actions.CRUD_CREATE,
    actions.CRUD_CREATE_SUCCESS,
    actions.CRUD_CREATE_FAIL
  ];
  const meta = {
    verb: "post",
    bodyInput,
    collection,
    clientId,
    url,
    ...options
  };

  return dispatch(post({ url, types, meta, bodyInput }));
};

export const makeUpdate = (
  collection,
  uri,
  options = {},
  bodyInput = {},
  clientId
) => dispatch => {
  const url = createUrl(uri);
  const types = [
    actions.CRUD_UPDATE,
    actions.CRUD_UPDATE_SUCCESS,
    actions.CRUD_UPDATE_FAIL
  ];
  const meta = {
    verb: "put",
    id: bodyInput.id,
    bodyInput,
    collection,
    clientId,
    url,
    ...options
  };

  return dispatch(put({ url, types, meta, bodyInput }));
};

export const makeArchive = (
  collection,
  uri,
  options = {},
  bodyInput,
  clientId,
  restore
) => dispatch => {
  const url = createUrl(uri);
  const types = [
    actions.CRUD_UPDATE,
    actions.CRUD_UPDATE_SUCCESS,
    actions.CRUD_UPDATE_FAIL
  ];
  const meta = {
    verb: "put",
    bodyInput,
    collection,
    clientId,
    url,
    ...options
  };

  return dispatch(putArchive({ url, types, meta, bodyInput, restore }));
};

export const makeRestore = (collection, uri, options, bodyInput, clientId) =>
  makeArchive(collection, uri, options, bodyInput, clientId, true);

export const makeDelete = (
  collection,
  uri,
  options = {},
  id,
  clientId
) => dispatch => {
  const url = createUrl(uri);
  const types = [
    actions.CRUD_DELETE,
    actions.CRUD_DELETE_SUCCESS,
    actions.CRUD_DELETE_FAIL
  ];
  const meta = {
    verb: "del",
    id,
    collection,
    clientId,
    url,
    ...options
  };

  return dispatch(del({ url, types, meta, id }));
};

function getVerb(request) {
  switch (request) {
    case "CREATE":
      return "post";
    case "UPDATE":
      return "put";
    case "DELETE":
      return "del";
    default:
      return "get";
  }
}

// Used by tz-store wrapper.
export function updateActionFactory(
  collection,
  payload,
  request = "FETCH",
  status = "SUCCESS"
) {
  const type = actions[`CRUD_${request}_${status}`];
  const meta = {
    verb: getVerb(request),
    collection,
    id: payload && payload.id
  };
  return {
    type,
    payload,
    meta
  };
}

export function rawFilter(inputFilter) {
  return {
    ...inputFilter,
    __raw_query: true
  };
}
