const DEFAULT_TIMEOUT = 15000;

export const xhr = (options, cb) => {
  return new Promise(function(resolve, reject) {
    const req = new XMLHttpRequest();

    if (typeof options === "string") {
      options = { url: options };
    }

    const rawData = (options && options.rawData) || false;

    req.addEventListener("load", function() {
      if (req.status === 200) {
        const response = rawData
          ? this.response
          : JSON.parse(this.responseText);
        resolve(response);
        cb && cb(null, response);
      } else if (req.status === 204) {
        resolve({});
        cb && cb(null, {});
      } else {
        reject(req.status);
        cb && cb(req.status);
      }
    });

    req.addEventListener("error", function() {
      reject(req.status);
      cb && cb(req.status);
    });

    req.addEventListener("abort", function() {
      reject("aborted");
      cb && cb("aborted");
    });

    req.addEventListener("timeout", function() {
      reject("timeout");
      cb && cb("timeout");
    });

    const type = (options && options.type) || "GET";
    const url = (options && options.url) || "";
    req.open(type, url, true);
    req.timeout = (options && options.timeout) || DEFAULT_TIMEOUT;

    let data = (options && options.data) || null;
    if (data && !rawData && typeof data !== "string") {
      data = JSON.stringify(data);
    }
    if (data && !rawData) {
      const contentType =
        (options && options.contentType) || "application/json; charset=utf-8";
      req.setRequestHeader("Content-Type", contentType);
    }
    req.send(data);
  });
};
