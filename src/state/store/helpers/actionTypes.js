const _dirname = "@CRUD/";

// Action types
export const CRUD_CREATE = `${_dirname}CREATE`;
export const CRUD_CREATE_SUCCESS = `${_dirname}CREATE_SUCCESS`;
export const CRUD_CREATE_FAIL = `${_dirname}CREATE_FAIL`;

export const CRUD_FETCH = `${_dirname}FETCH`;
export const CRUD_FETCH_SUCCESS = `${_dirname}FETCH_SUCCESS`;
export const CRUD_FETCH_FAIL = `${_dirname}FETCH_FAIL`;

export const CRUD_UPDATE = `${_dirname}UPDATE`;
export const CRUD_UPDATE_SUCCESS = `${_dirname}UPDATE_SUCCESS`;
export const CRUD_UPDATE_FAIL = `${_dirname}UPDATE_FAIL`;

export const CRUD_DELETE = `${_dirname}DELETE`;
export const CRUD_DELETE_SUCCESS = `${_dirname}DELETE_SUCCESS`;
export const CRUD_DELETE_FAIL = `${_dirname}DELETE_FAIL`;
