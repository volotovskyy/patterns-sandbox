import _ from "lodash";
import isFunction from "lodash/isFunction";
import endsWith from "lodash/endsWith";
import { xhr } from "./xhr";
import Immutable from "immutable";
// import * as records from "./records";
import rootReducer from "../reducer";

export function chaosMonkey(callback) {
  return callback;
}

const TIMEZYNK_REST = "https://pokeapi.co/api/v2";
// const TIMEZYNK_REST = "http://swapi.co/api";

// TODO: Enable records again when they are more tested.
function getRecord(collection) {
  const Record = rootReducer[collection];

  if (!Record) {
    // console.log('No record defined for ' + collection);
    return null;
  }

  return Record;
}

export function toRecord(collection, res) {
  if (!res) {
    return res;
  }

  const Record = getRecord(collection, res);
  if (!Record) {
    return Immutable.fromJS(res);
  }
  return Array.isArray(res)
    ? Immutable.List(res.map(i => new Record(i)))
    : new Record(res);
}

function batchParsing(collection, result) {
  return new Promise(resolve => {
    // Batch parsing of large collections to avoid script freeze!

    let i = 0;
    let list = Immutable.List().asMutable();
    const chunks = _.chunk(result, 200);

    function onFinished() {
      resolve(list.asImmutable());
    }

    function parseChunk() {
      const chunk = chunks[i];
      const recs = toRecord(collection, chunk);
      if (recs) {
        list = list.concat(recs);
      }
      i += 1;

      if (i < chunks.length) {
        requestAnimationFrame(parseChunk);
      } else {
        requestAnimationFrame(onFinished);
      }
    }

    parseChunk();
  });
}

function xhrPromise(options, collection) {
  return new Promise(
    chaosMonkey((resolve, reject) => {
      xhr(options).then(
        result => {
          const data = collection ? toRecord(collection, result) : result;
          resolve(data);
        },
        err => {
          console.warn(`API Error: ${err}`);
          reject(err);
        }
      );
    })
  );
}

// exports:

export function fetch(url, { collection }) {
  return new Promise(
    chaosMonkey((resolve, reject) => {
      xhr(`${TIMEZYNK_REST}${url}`).then(result => {
        if (!Array.isArray(result)) {
          resolve(toRecord(collection, result));
        } else {
          batchParsing(collection, result).then(resolve, reject);
        }
      }, reject);
    })
  );
}

export function create(url, data, { collection }) {
  return xhrPromise(
    {
      url: TIMEZYNK_REST + url,
      type: "POST",
      data: JSON.stringify(data)
    },
    collection
  );
}

export function update(url, item, { collection }) {
  const id = isFunction(item.get) ? item.get("id") : item.id || "";
  if (id && !endsWith(url, "/")) {
    url += "/";
  }

  return xhrPromise(
    {
      url: TIMEZYNK_REST + url + id,
      type: "PUT",
      data: JSON.stringify(item)
    },
    collection
  );
}

export function archive(url, data, { collection }, restore = false) {
  const action = restore ? "restore" : "archive";
  return xhrPromise(
    {
      url: `${TIMEZYNK_REST}/${action}${url}`,
      type: "PUT",
      data: data ? JSON.stringify(data) : null
    },
    collection
  );
}

export function destroy(url, id = null, data = null, vid = null) {
  let fullUrl = `${TIMEZYNK_REST}${url}`;
  if (id) {
    if (!endsWith(fullUrl, "/")) {
      fullUrl += "/";
    }
    fullUrl += id;
  }

  if (id && vid) {
    fullUrl += `?vid=${vid}`;
  }

  return xhrPromise({
    url: fullUrl,
    type: "DELETE",
    data: data ? JSON.stringify(data) : null
  });
}
