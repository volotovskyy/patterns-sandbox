import isString from 'lodash/isString';
import isEmpty from 'lodash/isEmpty';
import clone from 'lodash/clone';
import extend from 'lodash/extend';
import isFunction from 'lodash/isFunction';
import dateFormat from 'dateformat-light';
import { parseISODate, addDays } from 'tzdateutils';

function prepDate(o, k) {
    let v = o && o[k];
    if (!v) {
        return null;
    }
    if (isString(v)) {
        v = v.trim();
        if (isEmpty(v)) {
            return null;
        }
        return v;
    }
    return dateFormat(v, 'isoDate');
}

function formatQuery(inputQuery) {
    if (!inputQuery || (typeof (inputQuery) === 'object' && isEmpty(inputQuery))) {
        return '';
    }

    let query = clone(inputQuery, true);
    if (query.__raw_query) {
        delete query.__raw_query;
        return query;
    }

    const from = prepDate(query, 'from');
    const to = prepDate(query, 'to');
    const startFrom = prepDate(query, 'startFrom');
    const startTo = prepDate(query, 'startTo');

    delete query.from;
    delete query.to;
    delete query.startFrom;
    delete query.startTo;

    if ((from || to) && (startFrom || startTo)) {
        throw 'You can not combine from or to with startFrom or startTo';
    }

    if (from && to) {
        query = extend(query, {
            start: { _lt_: to },
            _or_: [
                { end: { _gt_: from } },
                { start: { _gte_: from } },
            ],
        });
        if (isFunction(parseISODate)) {
            const fromDate = parseISODate(from);
            const beforeFrom = fromDate && addDays(fromDate, -1);
            if (beforeFrom) {
                query.end = { _gt_: dateFormat(beforeFrom, 'isoDate') };
            }
        }
        return query;
    } else if (from) {
        return extend(query, {
            start: { _gte_: from },
        });
    } else if (to) {
        return extend(query, {
            start: { _lt_: to },
        });
    } else if (startFrom && startTo) {
        return extend(query, {
            start: {
                _gte_: startFrom,
                _lt_: startTo,
            },
        });
    } else if (startFrom) {
        return extend(query, {
            start: { _gte_: startFrom },
        });
    } else if (startTo) {
        return extend(query, {
            start: { _lt_: startTo },
        });
    }
    return query;
}

function createUrl(path, query) {
    let queryStr = formatQuery(query);
    if (isEmpty(queryStr)) {
        queryStr = '';
    } else if (typeof queryStr === 'string') {
        queryStr = `?${encodeURIComponent(queryStr)}`;
    } else {
        queryStr = `?q=${encodeURIComponent(JSON.stringify(queryStr))}`;
    }
    return path + queryStr;
}

export default createUrl;
