/*
 * Here we import the composed reducer, define & bootstrap middlewares and create
 * the store.
 */

import { compose, createStore, applyMiddleware } from "redux";
// import thunk from "redux-thunk";

import apiClientMiddleware from "./middlewares/apiClientMiddleware";
// import filtertabsMiddleware from 'state/middleware/filtertabs-middleware';
import enableBatching from "./helpers/batchActions";
import reducer from "./reducer";

const middleware = [
  apiClientMiddleware()
  // thunk
  // filtertabsMiddleware(),
];

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const enhancer = composeEnhancers(applyMiddleware(...middleware));

const store = createStore(
  enableBatching(reducer), // lets us batch actions
  enhancer
);

export default store;
