import { useMemo } from "react";

export function useSuspense(fn, input) {
  const memoizedResource = useMemo(() => {
    const resource = { type: "Pending", value: null };
    resource.value = Promise.resolve(fn(...input))
      .then(result => Object.assign(resource, { type: "Resolved", value: result }))
      .catch(error => Object.assign(resource, { type: "Rejected", value: error }));

    return resource;
  }, [fn, input]);

  return memoizedResource;
}
