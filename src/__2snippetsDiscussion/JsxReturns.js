import React from 'react';

export const JsxReturns = () => {
    
    return (
        repsByUserId &&
        repsByUserId
            .map((userReports, index) => (
                <div className="mb-3" key={index}>
                    <UserCard small userId={userId} className="border-bottom pb-2 mb-2" />
                    <SummaryCols
                        reports={userReports}
                        userId={userId}
                        byArt={byArt}
                        Preview={UserPreview}
                    />
                </div>
            ))
            .toList()
    );
}

// should be repsByUserId ? repsByUserId.map() : null since it will break if repsByUserId undefined as this is invalid JSX return