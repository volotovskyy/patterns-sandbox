import React from "react";
import { Provider } from "react-redux";
import { ThemeProvider } from "styled-components";
// import { GlobalStyle, theme } from "styles";
import { Swapi } from "modules/Swapi/components";
import { StyledSystem } from "modules/StyledSystem";
import styled, { css } from "styled-components";

import store from "state/store";

// own
import { theme } from "./styles/theme";
import "./styles/App.scss";

const darkModeStyles = css`
  background: darkblue;
  padding: 20px;


`;

const Button = styled.div`
  text-transform: uppercase;
  ${props => {
    console.log("props", props);
    return props.darkMode
      ? darkModeStyles
      : `background: ${props.theme.colors.green}`;
  }}
`;

function App() {
  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <>
          {/* <GlobalStyle /> */}
          <div className="App d-flex">Modules</div>
          <Button className="py-2" darkMode={false}>
            BS4 Button
          </Button>
          <hr />
          <StyledSystem />
          <hr />
          <Swapi />
        </>
      </ThemeProvider>
    </Provider>
  );
}

export default App;
