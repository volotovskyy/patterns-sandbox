# Sandbox with patters

1. get rid of inline api call and async operation on component level
2. redux-thunk, async/await, API utils
3. hooks impact
4. example of module-like structure
5. styles

Also: logging, testing, code conventions and more!
